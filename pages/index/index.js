Page({

  /**
   * 页面的初始数据
   */
  data: {
    region:['广东省','东莞市','厚街镇'],
    now:{"cloud":"10","cond_code":"999",
"cond_txt":"未知","fl":0,"hum":0,"pcpn":"0.0","pres":0,
"tmp":0,"vis":0,"wind_deg":0,"wind_dir":0,
"wind_sc":"0","wind_spd":"0"}
  },

  // 区域选择
  regionChange(e){
    this.setData({region:e.detail.value});
    this.getWeather();
  },

// 获取实时天气数据
getWeather(){
  var that = this;
  wx.request({
    url: 'https://free-api.heweather.com/s6/weather/now',
    data:{
      location:that.data.region[1],
      key:'de4403b51d51429688eba87a5624271e'
    },
    success(res){
      that.setData({now:res.data.HeWeather6[0].now});
    }
  })
},


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getWeather();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})